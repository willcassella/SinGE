// System.h
#pragma once

#include <Core/Reflection/Reflection.h>
#include "../config.h"

namespace singe
{
	struct ENGINE_API System final
	{
		REFLECTED_INTERFACE;
		AUTO_IMPL_0(System)

		/////////////////////
		///   Functions   ///
	public:

		
	};
}
