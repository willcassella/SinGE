# SinGE
## A game engine experiment

This project is largely inspired by ideas I had/things I learned while developing WillowEngine, I'm not sure how far I'll go with it. Currently it's written in a more C-style C++ than WillowEngine, as I'd like to keep things somewhat simpler and make it easier to create bindings for other languages. Eventually I'd like to introduce Rust somewhere, whether as the foundation or as another layer I'm not sure yet.